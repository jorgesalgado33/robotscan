import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';

import 'Product.dart';

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await initDB();
    return _database;
  }

  initDB() async {
    return await openDatabase(
      join(await getDatabasesPath(), 'products.db'),
      onCreate: (db, version) async {
        await db.execute("CREATE TABLE Product ("
            "id INTEGER PRIMARY KEY,"
            "nameProduct TEXT,"
            "data1 INT,"
            "data2 INT,"
            "data3 INT,"
            "data4 INT,"
            "data5 INT,"
            "data6 INT,"
            "url TEXT,"
            "blocked BIT"
            ")");
      },
      version: 1,
    );
  }

  newProduct(Product newProduct) async {
    final db = await database;

    //get the biggest id in the table
    var table = await db.rawQuery("SELECT MAX(id)+1 as id FROM Product");
    int id = table.first["id"];
    //insert to the table using the new id
    var raw = await db.rawInsert(
        "INSERT Into Product (id,nameProduct, data1, data2, data3, data4, data5, data6, url, blocked)"
        " VALUES (?,?,?,?,?,?,?,?,?,?)",
        [
          id,
          newProduct.nameProduct,
          newProduct.data1,
          newProduct.data2,
          newProduct.data3,
          newProduct.data4,
          newProduct.data5,
          newProduct.data6,
          newProduct.url,
          newProduct.blocked
        ]);
    return raw;
  }

  Future<dynamic> getProduct(int id) async {
    final db = await database;
    var res = await db.query("Product", where: "id = ?", whereArgs: [id]);
    return res.isNotEmpty ? Product.fromMap(res.first) : Null;
    /*if (res.length == 0) {
      return null;
    } else {
      var resMap = res[0];
      return resMap.isNotEmpty ? resMap : Null;
    }*/
  }

  Future<List<Product>> getAllProductsfromCellphone() async {
    final db = await database;
    var res = await db.query("Product");

    List<Product> list =
        res.isNotEmpty ? res.map((c) => Product.fromMap(c)).toList() : [];
    return list;
  }

  Future<void> deleteProduct(String productname) async {
    final db = await database;

    var res = await db.delete(
      'Product',
      where: "nameProduct = ?",
      whereArgs: [productname],
    );
    return res;
  }

  updateproduct(Product newProduct) async {
    final db = await database;
    var res = await db.update("Product", newProduct.toMap(),
        where: "id = ?", whereArgs: [newProduct.id]);
    return res;
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete from Product");
  }
}
