/// Product.dart
import 'dart:convert';

Product clientFromJson(String str) {
  final jsonData = json.decode(str);
  return Product.fromMap(jsonData);
}

String clientToJson(Product data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Product {
  int id;
  String nameProduct;
  int data1;
  int data2;
  int data3;
  int data4;
  int data5;
  int data6;
  String url;
  bool blocked;

  Product({
    this.id,
    this.nameProduct,
    this.data1,
    this.data2,
    this.data3,
    this.data4,
    this.data5,
    this.data6,
    this.url,
    this.blocked,
  });

  factory Product.fromMap(Map<String, dynamic> json) => new Product(
        id: json["id"],
        nameProduct: json["nameProduct"],
        data1: json["data1"],
        data2: json["data2"],
        data3: json["data3"],
        data4: json["data4"],
        data5: json["data5"],
        data6: json["data6"],
        url: json["url"],
        blocked: json["blocked"] == 1,
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "nameProduct": nameProduct,
        "data1": data1,
        "data2": data2,
        "data3": data3,
        "data4": data4,
        "data5": data5,
        "data6": data6,
        "url": url,
        "blocked": blocked,
      };
}
