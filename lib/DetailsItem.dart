import 'package:flutter/material.dart';
import 'Product.dart';
import 'package:flutter_radar_chart/flutter_radar_chart.dart';

class DetailsItem extends StatefulWidget {
  final Product productSelected;

  const DetailsItem({this.productSelected});

  @override
  _DetailsItem createState() => new _DetailsItem();
}

class _DetailsItem extends State<DetailsItem> {
  bool darkMode = false;
  bool useSides = true;
  double numberOfFeatures = 6;
  String pathPicture;
  @override
  initState() {
    super.initState();
    String getId = widget.productSelected.nameProduct;
    print(getId);

    if (getId == 'BLACK ROBOT') {
      pathPicture = 'assets/images/blackrobot.jpg';
    } else if (getId == 'BASIC ROBOT') {
      pathPicture = 'assets/images/robotsimple.jpg';
    } else if (getId == 'SELF BALANCE') {
      pathPicture = 'assets/images/SBR.jpg';
    } else if (getId == 'SILVER ROBOT') {
      pathPicture = 'assets/images/silverrobot.jpg';
    } else {
      pathPicture = 'assets/images/silverrobot.jpg';
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const ticks = [7, 14, 21, 28, 35];
    var features = [
      "ATTACK",
      "DEFENCE",
      "POWER",
      "SPEED",
      "TECH",
      "HUMAIN",
    ];
    setState(() {
      numberOfFeatures = 6;
    });
    var a = widget.productSelected.data1;
    var b = widget.productSelected.data2;
    var c = widget.productSelected.data3;
    var d = widget.productSelected.data4;
    var e = widget.productSelected.data5;
    var f = widget.productSelected.data6;
    print('$a');
    var data = [
      //[10, 20, 28, 5, 16, 15],
      [a, b, c, d, e, f],
      [15, 1, 4, 14, 23, 10]
    ];

    features = features.sublist(0, numberOfFeatures.floor());
    data = data
        .map((graph) => graph.sublist(0, numberOfFeatures.floor()))
        .toList();

    return Scaffold(
        appBar: AppBar(
          title: Text("Details"),
        ),
        body: Container(
            child: Column(children: <Widget>[
          /*Image.network(
              "https://images.frandroid.com/wp-content/uploads/2018/09/flutter.png"),*/
          Image.asset(
            pathPicture,
            height: 462,
            width: 347,
          ),
          Divider(),
          Text(widget.productSelected.nameProduct),
          Divider(),
          Expanded(
            child: RadarChart.light(
              ticks: ticks,
              features: features,
              data: data,
              reverseAxis: true,
              useSides: useSides,
            ),
          ),
        ])));
  }
}
